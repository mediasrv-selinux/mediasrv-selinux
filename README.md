# SElinux policy module for shared media server files.

This policy creates new shared contexts for applications used on a media server.
Designed with plex, sonar, radarr, nzbget and transmission in mind.

Rather than use the `public_content_t` type which is also used by system
applications this policy creates a pair of file contexts modeled after the
public contexts, but used only by app policies written to use them:


`mediasrv_content_ro_t` A read only type.

`mediasrv_content_rw_t` A read/write type.

mmap permissions are split into different permissions which differs from how the `public_content_t` type
is implemented. These types are

`mediasrv_mmap_content_ro_t` Read only file type with memory mapping allowed.

`mediasrv_mmap_content_rw_t` Red write file type with memory mapping allowed.


## Usage

The use of this policy by related applications defaults to **off**. In order to
use it you must enable the assoicated boolean for the application.

For example, to enable plex to use this file context you must run the following command:

    # setsebool plex_use_mediasrv on

To use this effectively it must be enabled for **all** applications sharing data.

The primary purpose of this policy is to integrate the various media sever applications with a common
labeling context. If you choose to use this over `public_content_t` then only your media directory
needs to be labeled as one of the `mediasrv_content_*` types. You may do so like this:

    # semanage fcontext -a -t mediasrv_content_rw_t "/usr/share/my_media_library(/.*)?"

The `mediasrv_content_rw_t` type will be the most likely context label to be used when integrating
with sonarr, radarr etc. If you wish additional file permission separation for metadata you may
use the native plex labels for that purpose.

If you use the Plex DVR the native plex file context `plex_content_rw_t` should be used for it, and
not the `mediasrv_content_*` labels.

## Release notes

mmap permissions are removed from the common read and manage macros and must br expressy granted with
`meadiasrv_mmap_ro_shared_files` or `mediasrv_mmap_rw_shared_files`.

## Building

### Requirements

In order to build and install this policy module you will need the following
packages installed:

* make
* selinux-policy
* selinux-policy-devel
* policycoreutils-python for Red Hat/CentOS <= 7 and Fedora <=31
* python3-policycoreutils for Red Hat CentOS 8+ and Fedora 32+

### Build

	$ make

### Install

Installs the policy package and relabels Plex Media Server files to new file
contexts.

(as root)

	# make install


### Uninstall

Uninstalls the policy module from the system and relabels Plex Media Server
files to the default file contexts.

(as root)

	# make uninstall


## Policy Details

### Domain

SELinux uses domains to label processes. The domain of a confined process
defines what access controls the process is subjected to.

This policy defines the domain `mediasrv_t`

However this policy is intended to define only a shared file context and no
entrypoints for executables are provided.

### File COntexts

SELinux requires that files be labeled with a security context so it knows what
access controls to apply. This security context is usually stored in an extended
attribute on the file.

The security context of a file can be inspected by using the -Z option with
the ls command.

	$ ls -Z /media
	drwxr-xr-x. plex plex unconfined_u:object_r:plex_var_lib_t:s0 Library

A file's security context can be changed temporarily using `chcon`. These
changes will not survive a relabeling.

	$ ls -Z /var/lib/plexmediaserver
	drwxr-xr-x. plex plex unconfined_u:object_r:plex_var_lib_t:s0 Library

A file's security context can be changed temporarily using `chcon`. These
changes will not survive a relabeling.

	$ chcon -R /usr/share/my_media_library

A file's security context can be changed permanently by using `semanage fcontext`
to define the default security context for a given set of files and then using
`restorecon` to apply those labels.

	$ semanage fcontext -a -t plex_content_t "/usr/share/my_media_library(/.*)?"
	$ restorecon -v -R /usr/share/my_media_library

This policy module defines the following file contexts:

##### mediasrv\_content\_ro_t and mediasrv\_content\_rw\_t

These file contexts are for labeling media files and directories that processes
confined by any of the media server applications should be allowed to share,
`mediasrv_content_ro_t` or manage `mediasrv_content_rw_t`.
<br />
<br />
Always use `plex_content_rw_t` for DVR libraries.
<br />
<br />

##### Executable contexts

This policy defines no executable contexts.


### Networking

This policy defines no networking related contexts.

### Booleans

No booleans are defined by this policy.



