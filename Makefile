include /usr/share/selinux/devel/Makefile

.PHONY: install uninstall

install:
	semodule -v -i mediasrv.pp

uninstall:
	semodule -v -r mediasrv

